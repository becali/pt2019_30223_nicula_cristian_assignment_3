
import java.sql.*;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;



public class Adaugare extends JPanel {

	private static final long serialVersionUID = 1L;
	private String url = "jdbc:mysql://localhost/project";
	private String uid = "root";
	private String pw = "";
	private Connection con;
	
	private JTextArea tf_afisare = new JTextArea();
	private JTextField tf_client;
	
	private JButton b1 = new JButton("Insert");
	private JButton b2 = new JButton("client nou");
	private JButton b3 = new JButton("Afisare clienti");
	
	public void connectDB() {
		try { 
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (java.lang.ClassNotFoundException e) {
			System.err.println("ClassNotFoundException: " + e);
		}

		con = null;
		try {
			con = DriverManager.getConnection(url, uid, pw);
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex);
			System.exit(1);
		}
	}

	
	public void afisareclient() throws SQLException {
		Statement stmt = null;
		String sql = "SELECT id_client, nume_client, varsta FROM project.DATE_client";
		try {
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next())
			{
				String nume = rs.getString("nume_client");
				int varsta = rs.getInt("varsta");
				int id = rs.getInt("id_client");
				tf_afisare.setText(tf_afisare.getText() +id+". "+nume +", "+varsta +" ani\n");
			}
		} catch (SQLException e) {
			System.out.println("Eroare la inserare: " + e);
		} finally
		{
			if(stmt != null)
			{
				stmt.close();
			}
		}
	}
	
	JFrame inserare_client = new JFrame();
	JFrame principala = new JFrame();
	JFrame afisare_client = new JFrame();
	JFrame pagina_login = new JFrame();
	
	JPanel p = new JPanel();
	JPanel p_principal = new JPanel();
	JPanel p_afisare = new JPanel();
	JPanel p_login = new JPanel();
	
	TextField tf_nume = new TextField();
	TextField tf_parola = new TextField();
	
	String grupa;
	JLabel poza = new JLabel();
	JLabel poza2 = new JLabel();
	JLabel e_nume = new JLabel("Nume:");
	JLabel e_parola = new JLabel("Parola:");
	JLabel poza3 = new JLabel();
	
	JButton ok = new JButton("ok");
	
	TextField tf_nume_client = new TextField();
	TextField tf_CNP = new TextField();
	TextField tf_tel = new TextField();
	TextField tf_varsta = new TextField();
	
	String numele = null, CNP = null;
	String nr_tel = null;
	String varsta = null; 
	public Adaugare() {
		connectDB();
		
		ImageIcon bmw = new ImageIcon("bmw.jpg");
		poza.setIcon(bmw);
		poza.setBounds(0,0,800,400);
		
		ImageIcon sange = new ImageIcon("sange.jpg");
		poza3.setIcon(sange);
		poza3.setBounds(0,0,550,400);
		
		ImageIcon login = new ImageIcon("Harold.jpg");
		
		poza2.setIcon(login);
		poza2.setBounds(0,0,800,533);
		
		pagina_login.setSize(800,  533);
		p_login.setLayout(null);
		
		ok.setBounds(225, 265, 75, 50);
		tf_nume.setBounds(50, 100, 250, 50);
		tf_parola.setBounds(50, 200, 250, 50);
		e_nume.setBounds(50, 75, 100, 25);
		e_parola.setBounds(50, 175, 100, 25);
		
		e_nume.setFont(new Font("Arial", Font.PLAIN, 28));
		e_parola.setFont(new Font("Arial", Font.PLAIN, 28));
		tf_nume.setFont(new Font("Arial", Font.PLAIN, 28));
		tf_parola.setFont(new Font("Arial", Font.PLAIN, 28));
		
		p_login.add(e_nume);
		p_login.add(e_parola);
		p_login.add(ok);
		p_login.add(tf_nume);
		p_login.add(tf_parola);
		p_login.add(poza2);
		
		p_afisare.setLayout(null);
		
		
		inserare_client.setSize(500, 450);
		inserare_client.setVisible(false);
		
		afisare_client.setSize(800, 400);
		afisare_client.setVisible(false);
		
		Color aha = new Color(255,255,179);
		Color gri = new Color(255, 235, 230);
		
		principala.setSize(800,400);
		tf_afisare.setEditable(false);
		tf_afisare.setBounds(550, 0, 250, 400);
		tf_afisare.setFont(new Font("Arial", Font.PLAIN, 17));
		tf_afisare.setBackground(gri);
		
		JLabel poza4 = new JLabel();
		ImageIcon mda = new ImageIcon("donation.jpg");
		poza4.setIcon(mda);
		poza4.setBounds(0, 0, 500, 450);
		
		
		tf_client = new JTextField();
		
		

		p_afisare.add(poza3);
		p_afisare.add(tf_afisare);
		
		p_principal.setLayout(null);
		
		
		b2.setBounds(30, 275, 150, 55);
		b3.setBounds(30, 40, 150, 55);
		
		b2.setFont(new Font("Arial", Font.BOLD, 17));
		b3.setFont(new Font("Arial", Font.BOLD, 15));
		
		p.setLayout(null);
		
		JLabel Gr_label = new JLabel("Grupa");
		JLabel l1 = new JLabel("Nume");
		JLabel l2 = new JLabel("CNP");
		JLabel l3 = new JLabel("Varsta");
		JLabel l4 = new JLabel("Telefon");
		
		Gr_label.setBounds(25, 270, 75, 50);
		Gr_label.setFont(new Font("Arial", Font.BOLD, 25));
		
		l1.setBounds(25, 30, 125, 50);
		l2.setBounds(25, 90, 125, 50);
		l4.setBounds(25, 150, 125, 50);
		l3.setBounds(25, 210, 125, 50);
		
		l1.setFont(new Font("Arial", Font.BOLD, 25));
		l2.setFont(new Font("Arial", Font.BOLD, 25));
		l3.setFont(new Font("Arial", Font.BOLD, 25));
		l4.setFont(new Font("Arial", Font.BOLD, 25));
		
		p.add(l1);
		p.add(l2);
		p.add(l4);
		p.add(l3);
		
		tf_client.setBounds(200, 270, 200, 50);
		tf_client.setFont(new Font("Arial", Font.BOLD, 25));
		b1.setBounds(225, 320, 150, 40);
		
		tf_nume_client.setBounds(200, 30, 200, 50);
		tf_CNP.setBounds(200, 90, 200, 50);
		tf_tel.setBounds(200, 150, 200, 50);
		tf_varsta.setBounds(200,210,200,50);
		
		tf_nume_client.setFont(new Font("Arial", Font.BOLD, 25));
		tf_CNP.setFont(new Font("Arial", Font.BOLD, 25));
		tf_varsta.setFont(new Font("Arial", Font.BOLD, 25));
		tf_tel.setFont(new Font("Arial", Font.BOLD, 25));
		
		
		p.add(Gr_label);
		p.add(b1);
		p.add(tf_nume_client);
		p.add(tf_CNP);
		p.add(tf_varsta);
		p.add(tf_tel);
		p.add(tf_client);
		
		p_principal.add(b2);
		p_principal.add(b3);
		p_principal.add(poza);
		
		
		class ButtonListenerb1 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	grupa = tf_client.getText();
	        	numele = tf_nume_client.getText();
	        	CNP = tf_CNP.getText();
	        	nr_tel =(tf_tel.getText());
	        	varsta = (tf_varsta.getText());
	        	
	        	CallableStatement cstmt = null;
	        	CallableStatement cstmt2 = null;
	    		try {
	    			String sql = "{call IN_client_DONATIE(?)}";
	    			
	    			cstmt = con.prepareCall(sql);
	    			cstmt.setString(1, grupa);
	    			cstmt.execute();   			
	    			
	    			
	    			String sql2 = "CALL INSERT_DATE (?,?,?,?)";
	    			cstmt2 = con.prepareCall(sql2);
	    			cstmt2.setString(1, numele);
	    			cstmt2.setString(2, CNP);
	    			cstmt2.setString(3, varsta);
	    			cstmt2.setString(4, nr_tel);
	    			cstmt2.execute();  
	    			
	    			System.out.println("client inserat cu success!");
	    		} catch (SQLException ef) {
	    			System.out.println("Eroare la inserare " + e);
	    		}
	    		
	    	
	    			
	    		
	        }
	    }  
		
		class ButtonListenerb2 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	inserare_client.setVisible(true);
	        }
	    } 
		
		class ButtonListenerb3 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	try {
					afisareclient();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
	        	afisare_client.setVisible(true);
	        }
	    }
		
		class ButtonListenerok implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	if(tf_nume.getText().equals("Admin") && tf_parola.getText().equals("sqldb"))
	        	{
	        		principala.setVisible(true);
	        		pagina_login.setVisible(false);
	        	}
	        	else
	        	{
	        		tf_nume.setText(" ");
	        		tf_parola.setText(" ");
	        	}
	        }
	    } 
		
		ButtonListenerb1 da = new ButtonListenerb1();
		ButtonListenerb2 da2 = new ButtonListenerb2();
		ButtonListenerb3 da3 = new ButtonListenerb3();
		ButtonListenerok da4 = new ButtonListenerok();
		
		b1.addActionListener(da);
		b2.addActionListener(da2);
		b3.addActionListener(da3);
		ok.addActionListener(da4);
		
		p.add(poza4);
		
		inserare_client.setContentPane(p);
		p_principal.setBackground(aha);
		principala.setContentPane(p_principal);
		afisare_client.setContentPane(p_afisare);
		pagina_login.setContentPane(p_login);
		pagina_login.setVisible(true);
		principala.setVisible(false);
	}

	
	public static void main(String[] args)
	{
		@SuppressWarnings("unused")
		Adaugare a = new Adaugare();
		
	}
}